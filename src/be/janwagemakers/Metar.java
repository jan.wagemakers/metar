package be.janwagemakers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Metar {

    private int temperature;
    private int pressure;

    public void update(String airport) {
        try {
            URL url = new URL("https://tgftp.nws.noaa.gov/data/observations/metar/stations/" + airport + ".TXT");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            int counter = 0;
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                // System.out.println(counter + " " + inputLine);
                // inputLine="EBAW 112220Z AUTO 27003KT 9999 // FEW028/// M05/02 Q1032";
                if (counter == 1) {
                    for (int i = 0; i < inputLine.length() - 4; i++) {
                        char c = inputLine.charAt(i);
                        char c1 = inputLine.charAt(i + 1);
                        char c2 = inputLine.charAt(i + 2);
                        char c3 = inputLine.charAt(i + 3);
                        char c4 = inputLine.charAt(i + 4);
                        if ((c == ' ' || c == 'M') && Character.isDigit(c1) && Character.isDigit(c2) && c3 == '/') {
                            temperature = Integer.parseInt("" + c1 + c2);
                            if (c == 'M') temperature = temperature * -1;
                        }
                        if ((c == 'Q' || c == 'A') && Character.isDigit(c1) && Character.isDigit(c2) && Character.isDigit(c3) && Character.isDigit(c4)) {
                            pressure = Integer.parseInt("" + c1 + c2 + c3 + c4);
                            if (c == 'A') {
                                float p = pressure * 1.3333f * 25.4f / 100; // hg -> hectopascal / inch -> mm
                                pressure = Math.round(p);
                            }
                        }
                    }
                }
                counter++;
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }
}

