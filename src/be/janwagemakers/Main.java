package be.janwagemakers;

public class Main {

    public static void main(String[] args) {
        Metar metar = new Metar();
        metar.update("EBAW");
        System.out.println("T=" + metar.getTemperature() + "°C P=" + metar.getPressure() + "hPa");
    }
}
